package org.huatekon.utilities;

import org.powerbot.core.script.job.Task;
import org.powerbot.game.api.methods.Settings;
import org.powerbot.game.api.methods.Widgets;
import org.powerbot.game.api.wrappers.widget.WidgetChild;

public class ProductionDialog extends Task {

	private final int DIALOG_SETTINGS = 1173;
	private final int ON = 0x30000000;
	private final int OFF = 0x70000000;
	private final int PARENT_WIDGET_D = 548;
	private final int CHILD_WIDGET_D = 43;

	@Override
	public void execute() {
		if (Settings.get(DIALOG_SETTINGS) == OFF) {
			System.out.println("PDIALOG EXECUTE");
			WidgetChild dialog = Widgets.get(PARENT_WIDGET_D).getChild(CHILD_WIDGET_D);
			if (dialog != null) {
				if (dialog.interact("Toggle Production Dialog")) {
					Utilities.waitFor(new Condition() {
						@Override
						public boolean validate() {
							return Settings.get(DIALOG_SETTINGS) == ON;
						}
					}, 5000);
				}
			}
		}
	}
}
