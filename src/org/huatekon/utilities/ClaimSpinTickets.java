package org.huatekon.utilities;

import org.powerbot.core.randoms.SpinTickets;
import org.powerbot.game.api.methods.Settings;
import org.powerbot.game.api.methods.tab.Inventory;
import org.powerbot.game.api.methods.widget.Bank;
import org.powerbot.game.api.wrappers.node.Item;

public class ClaimSpinTickets extends SpinTickets{
	@Override
	public void execute() {
		if (Bank.isOpen()) {
			if(Bank.close()) {
				sleep(1000);
			}
		} else if (((Settings.get(1448) & 0xFF00) >>> 8) < 10) {
			final Item item = Inventory.getItem(24154, 24155);
			if (item != null && item.getWidgetChild().interact("Claim spin")) {
				sleep(1000);
			}
		} else {
			super.execute();
		}
	}
}
