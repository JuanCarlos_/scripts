package org.huatekon.crazyurns;

import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.SwingUtilities;

import org.huatekon.crazyurns.nodes.Craft;
import org.huatekon.crazyurns.nodes.Deposit;
import org.huatekon.crazyurns.nodes.FailSafe;
import org.huatekon.crazyurns.nodes.GetClay;
import org.huatekon.crazyurns.nodes.Oven;
import org.huatekon.crazyurns.nodes.WalkBank;
import org.huatekon.crazyurns.nodes.WalkCraft;
import org.huatekon.crazyurns.utilities.Gui;
import org.huatekon.crazyurns.utilities.Paint;
import org.huatekon.crazyurns.utilities.Variables;
import org.huatekon.utilities.ClaimSpinTickets;
import org.huatekon.utilities.ProductionDialog;
import org.powerbot.core.event.events.MessageEvent;
import org.powerbot.core.event.listeners.MessageListener;
import org.powerbot.core.event.listeners.PaintListener;
import org.powerbot.core.randoms.SpinTickets;
import org.powerbot.core.script.ActiveScript;
import org.powerbot.core.script.job.Task;
import org.powerbot.core.script.job.state.Node;
import org.powerbot.core.script.job.state.Tree;
import org.powerbot.game.api.Manifest;
import org.powerbot.game.api.methods.Environment;
import org.powerbot.game.api.methods.Game;
import org.powerbot.game.api.util.Random;

@Manifest(authors = { "huatekon" },
name = "CrazyUrns",
version = 4.1,
description = "Craft all kind of urns at varrok west bank for profit, Toogle Production Dialog ON!.",
website = "http://www.powerbot.org/community/topic/728819-crazyurns/")
public class CrazyUrns extends ActiveScript implements MessageListener,
		PaintListener, MouseListener {

	private Tree jobs = null;
	private static final ClaimSpinTickets ticketRandom = new ClaimSpinTickets();
	
	@Override
	public void onStart() {
		getContainer().submit(new Task() {
			@Override
			public void execute() {
				sleep(5000);
				Environment.enableRandom(SpinTickets.class, false);
			}
		});
		
		getContainer().submit(new ProductionDialog());
		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				Gui gui = new Gui();
				gui.setVisible(true);
			}
		});

		try {
			Variables.paint = ImageIO.read(new URL(
					"http://dl.dropbox.com/u/15725670/crazyurns.jpg"));
		} catch (Exception e) {
			// error
		}
	};

	@Override
	public int loop() {
		if(ticketRandom.activate()){
			ticketRandom.execute();
			return 100;
		}
		
		if (jobs == null) {
			jobs = new Tree(new Node[] {new GetClay(), new WalkCraft(),
					new Craft(), new Oven(), new WalkBank(), new Deposit(),
					new FailSafe(), });
		}

		if (Game.getClientState() == Game.INDEX_MAP_LOADED) {
			final Node node = jobs.state();
			if (node != null) {
				jobs.set(node);
				getContainer().submit(node);
				node.join();
			}
		}
		
		if (Variables.stop)
			return -1;

		return Random.nextInt(200, 300);
	}

	@Override
	public void messageReceived(MessageEvent m) {
		if (m.getMessage().startsWith("You remove the")) {
			Variables.urnsMade++;
		}
	}

	@Override
	public void onRepaint(Graphics g) {
		Paint.onRepaint(g);
	}

	@Override
	public void mouseClicked(MouseEvent me) {
		if (Variables.rect.contains(me.getPoint()) && !Variables.hidepaint) {
			Variables.hidepaint = true;
		} else {
			Variables.hidepaint = false;
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

}
