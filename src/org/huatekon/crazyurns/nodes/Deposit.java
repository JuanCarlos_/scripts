package org.huatekon.crazyurns.nodes;

import org.huatekon.crazyurns.utilities.Variables;
import org.huatekon.utilities.Utilities;
import org.powerbot.core.script.job.state.Node;
import org.powerbot.game.api.methods.interactive.Players;
import org.powerbot.game.api.methods.node.SceneEntities;
import org.powerbot.game.api.methods.tab.Inventory;
import org.powerbot.game.api.methods.widget.Bank;
import org.powerbot.game.api.wrappers.Area;
import org.powerbot.game.api.wrappers.Tile;
import org.powerbot.game.api.wrappers.node.SceneObject;

public class Deposit extends Node {

	public static final Area BANKAREA = new Area(new Tile(3250, 3424, 0),
			new Tile(3257, 3419, 0));
	public static final int BANK_BOOTH = 782;

	@Override
	public boolean activate() {
		return Variables.started && Players.getLocal().getPlane() != 1
				&& BANKAREA.contains(Players.getLocal().getLocation())
				&& Inventory.getCount(Variables.URNNR) > 0;
	}

	@Override
	public void execute() {
		Variables.status = "Depositing";
		final SceneObject bank = SceneEntities.getNearest(BANK_BOOTH);
		if (bank != null) {
			if(Utilities.openBank(bank))
				Bank.depositInventory();
		}
	}

}
