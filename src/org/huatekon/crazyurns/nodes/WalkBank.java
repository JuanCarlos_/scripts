package org.huatekon.crazyurns.nodes;

import org.huatekon.crazyurns.utilities.Variables;
import org.huatekon.utilities.Condition;
import org.huatekon.utilities.Utilities;
import org.powerbot.core.script.job.state.Node;
import org.powerbot.game.api.methods.Walking;
import org.powerbot.game.api.methods.interactive.Players;
import org.powerbot.game.api.methods.tab.Inventory;
import org.powerbot.game.api.util.Random;
import org.powerbot.game.api.wrappers.Tile;

public class WalkBank extends Node {

	public static final int DOOR = 24381;
	public static final Tile DOOR_LOCATION = new Tile(3259, 3400, 0);
	private static final Tile[] destination = { new Tile(3254, 3420, 0),
			new Tile(3253, 3420, 0), new Tile(3254, 3421, 0),
			new Tile(3253, 3421, 0) };

	@Override
	public boolean activate() {
		return Variables.started && Players.getLocal().getLocation().getPlane() != 1
				&& !Deposit.BANKAREA.contains(Players.getLocal().getLocation())
				&& Inventory.getCount(Variables.URNUNF) == 0
				&& Inventory.getCount(GetClay.SOFT_CLAY) <= 1;
	}

	@Override
	public void execute() {
		Variables.status = "Walking to bank";
		Utilities.checkRun();
		Utilities.openDoor(DOOR, DOOR_LOCATION);
		Walking.walk(destination[Random.nextInt(0, destination.length)]);
		if (Deposit.BANKAREA.contains(Walking.getDestination())) {
			Utilities.waitFor(new Condition() {
				@Override
				public boolean validate() {
					return Deposit.BANKAREA.contains(Players.getLocal()
							.getLocation());
				}
			}, Random.nextInt(3000, 4000));
		} else {
			Utilities.waitFor(new Condition() {

				@Override
				public boolean validate() {
					return Walking.getDestination().distance(
							Players.getLocal().getLocation()) < 7;
				}
			}, Random.nextInt(1000, 3000));
		}
	}

}
