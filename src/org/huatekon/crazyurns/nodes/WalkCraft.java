package org.huatekon.crazyurns.nodes;

import org.huatekon.crazyurns.utilities.Variables;
import org.huatekon.utilities.Condition;
import org.huatekon.utilities.Utilities;
import org.powerbot.core.script.job.state.Node;
import org.powerbot.game.api.methods.Walking;
import org.powerbot.game.api.methods.interactive.Players;
import org.powerbot.game.api.methods.tab.Inventory;
import org.powerbot.game.api.util.Random;
import org.powerbot.game.api.wrappers.Tile;

public class WalkCraft extends Node {

	private final Tile[] destination = { new Tile(3262, 3400, 0),
			new Tile(3261, 3400, 0), new Tile(3262, 3399, 0),
			new Tile(3261, 3399, 0) };

	@Override
	public boolean activate() {
		return Variables.started
				&& Players.getLocal().getLocation().getPlane() != 1
				&& !Craft.CRAFTERAREA
						.contains(Players.getLocal().getLocation())
				&& (Inventory.getCount(GetClay.SOFT_CLAY) >= 2 || Inventory
						.getCount(Variables.URNUNF) > 0);
	}

	@Override
	public void execute() {
		Variables.status = "Walking to wheel";
		Utilities.checkRun();
		Walking.walk(destination[Random.nextInt(0, destination.length)]);
		if (Craft.CRAFTERAREA.contains(Walking.getDestination())) {
			Utilities.waitFor(new Condition() {

				@Override
				public boolean validate() {
					return Craft.CRAFTERAREA.contains(Players.getLocal()
							.getLocation());
				}
			}, Random.nextInt(2000, 4000));
		}
		if (Utilities.openDoor(WalkBank.DOOR, WalkBank.DOOR_LOCATION)) {
			execute();
		}
	}

}
