package org.huatekon.crazyurns.nodes;

import java.awt.Point;

import org.huatekon.crazyurns.utilities.Variables;
import org.huatekon.utilities.Condition;
import org.huatekon.utilities.Utilities;
import org.powerbot.core.script.job.state.Node;
import org.powerbot.game.api.methods.Settings;
import org.powerbot.game.api.methods.Widgets;
import org.powerbot.game.api.methods.input.Mouse;
import org.powerbot.game.api.methods.interactive.Players;
import org.powerbot.game.api.methods.node.SceneEntities;
import org.powerbot.game.api.methods.tab.Inventory;
import org.powerbot.game.api.util.Random;
import org.powerbot.game.api.wrappers.Area;
import org.powerbot.game.api.wrappers.Tile;
import org.powerbot.game.api.wrappers.node.SceneObject;
import org.powerbot.game.api.wrappers.widget.WidgetChild;

public class Craft extends Node {

	public static final Area CRAFTERAREA = new Area(new Tile(3264, 3404, 0),
			new Tile(3260, 3397, 0));
	private final int WHEEL = 5092;
	private final int SETTING_FOR_URNS = 1170;
	public static WidgetChild WIDGET_URN_TITLE = Widgets.get(1370)
			.getChild(56);
	public static WidgetChild WIDGET_ACTION_BUTTON = Widgets.get(1370)
			.getChild(38);
	public static WidgetChild WIDGET_PRODUCTION_DIALOG = Widgets
			.get(1251).getChild(10);
	public static WidgetChild WIDGET_URN_CATEGORIES = Widgets.get(1371).getChild(51).getChild(0);

	@Override
	public boolean activate() {
		return Variables.started
				&& CRAFTERAREA.contains(Players.getLocal().getLocation())
				&& Inventory.getCount(GetClay.SOFT_CLAY) > 1;
	}

	@Override
	public void execute() {
		Variables.status = "Crafting";
		SceneObject potterWheel = SceneEntities.getNearest(WHEEL);
		if (potterWheel != null) {
			Utilities.focus(potterWheel);
			if (potterWheel.interact("Form")) {
				Utilities.waitFor(new Condition() {
					@Override
					public boolean validate() {
						return WIDGET_ACTION_BUTTON.isOnScreen();
					}
				}, 3000);
				if (WIDGET_ACTION_BUTTON.isOnScreen()) {
					selectCategory();
					if (Settings.get(SETTING_FOR_URNS) == Variables.urnId
							&& WIDGET_URN_TITLE.getText().equals(
									Variables.urnName)) {
						mould();
					}
				}
			}
		}
	}

	/**
	 * Selects a category from crafting UI.
	 */
	private void selectCategory() {
		//System.out.println("1");
		if (Widgets.get(1371).getChild(51).isOnScreen()) {
			//System.out.println("2");
			loadWidgets();
			
			if (!WIDGET_URN_CATEGORIES.getText()
					.equals(Variables.categoryName)) {
			//	System.out.println("3");
				if (Widgets.get(1371).getChild(51).click(true)) {
				//	System.out.println("4");
					if (Widgets.get(1371).getChild(60).isOnScreen()) {
					//	System.out.println("5");
						Point point;
						switch (Variables.urnCategory) {
						case 1:
							point = new Point(Random.nextInt(24, 217),
									Random.nextInt(132, 142));
							Mouse.click(point, true);
							break;
						case 2:
							point = new Point(Random.nextInt(24, 217),
									Random.nextInt(147, 157));
							Mouse.click(point, true);
							break;
						case 3:
							point = new Point(Random.nextInt(24, 217),
									Random.nextInt(162, 172));
							Mouse.click(point, true);
							break;
						case 4:
							point = new Point(Random.nextInt(24, 217),
									Random.nextInt(177, 187));
							Mouse.click(point, true);
							break;
						case 5:
							point = new Point(Random.nextInt(24, 217),
									Random.nextInt(192, 202));
							Mouse.click(point, true);
							break;
						case 6:
							point = new Point(Random.nextInt(24, 217),
									Random.nextInt(207, 217));
							Mouse.click(point, true);
							break;
						default:
							break;
						}
						Utilities.waitFor(new Condition() {
							@Override
							public boolean validate() {
								loadWidgets();
								return WIDGET_URN_TITLE.getText().equals(
										Variables.urnName);
							}
						}, 2000);
					}
				}
			}
			if (!WIDGET_URN_TITLE.getText().equals(Variables.urnName)) {
				selectUrn(Variables.urnId);
			}
		}
	}

	/**
	 * Select urn given id
	 * 
	 * @param id
	 *            This is a {@link Settings}
	 */
	private void selectUrn(int id) {
		//System.out.println("selecting urn");
		loadWidgets();
		if (WIDGET_ACTION_BUTTON.isOnScreen()) {
			if (Settings.get(SETTING_FOR_URNS) != id
					|| !WIDGET_URN_TITLE.getText().equals(Variables.urnName)) {
				Widgets.get(1371).getChild(44).getChild(Variables.urnWidgetId)
						.click(true);
			}
		}
	}

	/**
	 * Click mould widget button and wait until is finished
	 */
	private void mould() {
		if (WIDGET_ACTION_BUTTON.isOnScreen()) {
			WIDGET_ACTION_BUTTON.click(true);
			Utilities.waitFor(new Condition() {
				@Override
				public boolean validate() {
					return WIDGET_PRODUCTION_DIALOG.isOnScreen();
				}
			}, 5000);
			Utilities.waitFor(new Condition() {
				@Override
				public boolean validate() {
					Utilities.antiban();
					return !WIDGET_PRODUCTION_DIALOG.isOnScreen();
				}
			}, 20000);
		}
	}
	
	private void loadWidgets(){
		WIDGET_URN_TITLE = Widgets.get(1370)
				.getChild(56);
		WIDGET_ACTION_BUTTON = Widgets.get(1370)
				.getChild(38);
		WIDGET_PRODUCTION_DIALOG = Widgets
				.get(1251).getChild(10);
		WIDGET_URN_CATEGORIES = Widgets.get(1371).getChild(51).getChild(0);
	}
}
