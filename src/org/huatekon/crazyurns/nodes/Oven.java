package org.huatekon.crazyurns.nodes;

import org.huatekon.crazyurns.utilities.Variables;
import org.huatekon.utilities.Condition;
import org.huatekon.utilities.Utilities;
import org.powerbot.core.script.job.state.Node;
import org.powerbot.game.api.methods.interactive.Players;
import org.powerbot.game.api.methods.node.SceneEntities;
import org.powerbot.game.api.methods.tab.Inventory;
import org.powerbot.game.api.wrappers.node.SceneObject;

public class Oven extends Node {
	private final int OVEN = 5087;

	@Override
	public boolean activate() {
		return Variables.started
				&& Craft.CRAFTERAREA.contains(Players.getLocal().getLocation())
				&& Inventory.getCount(Variables.URNUNF) > 0
				&& Inventory.getCount(GetClay.SOFT_CLAY) <= 1;
	}

	@Override
	public void execute() {
		Variables.status = "Urns on fire";
		Utilities.minActionBar();
		SceneObject oven = SceneEntities.getNearest(OVEN);
		Utilities.focus(oven);
		if (oven.interact("Fire")) {
			Utilities.waitFor(new Condition() {

				@Override
				public boolean validate() {
					return Craft.WIDGET_ACTION_BUTTON.isOnScreen();
				}
			}, 4000);
			if(Craft.WIDGET_ACTION_BUTTON.isOnScreen()){
				if (Craft.WIDGET_ACTION_BUTTON.click(true)) {
					Utilities.waitFor(new Condition() {
						@Override
						public boolean validate() {
							return Craft.WIDGET_PRODUCTION_DIALOG.isOnScreen();
						}
					}, 3000);
					Utilities.waitFor(new Condition() {
						@Override
						public boolean validate() {
							Utilities.antiban();
							return !Craft.WIDGET_PRODUCTION_DIALOG.isOnScreen();
						}
					}, 60000);
				}
			}
		}
	}

}
