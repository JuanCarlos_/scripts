package org.huatekon.crazyurns.nodes;

import org.huatekon.crazyurns.utilities.Variables;
import org.huatekon.utilities.Utilities;
import org.powerbot.core.script.job.state.Node;
import org.powerbot.game.api.methods.interactive.Players;
import org.powerbot.game.api.methods.node.SceneEntities;
import org.powerbot.game.api.methods.tab.Inventory;
import org.powerbot.game.api.methods.widget.Bank;
import org.powerbot.game.api.wrappers.node.SceneObject;

public class GetClay extends Node {

	public static final int SOFT_CLAY = 1761;

	@Override
	public boolean activate() {
		return Variables.started && Players.getLocal().getPlane() != 1
				&& Inventory.getCount() <= 1
				&& Deposit.BANKAREA.contains(Players.getLocal().getLocation());
	}

	@Override
	public void execute() {
		Variables.status = "Getting soft clay";
		final SceneObject bank = SceneEntities.getNearest(Deposit.BANK_BOOTH);
		if (bank != null) {
			if (Utilities.openBank(bank)) {
				if (Bank.getItemCount(SOFT_CLAY) == 0) {
					Variables.stop = true;
				}
				Bank.withdraw(SOFT_CLAY, 28);
			}
		}
	}

}