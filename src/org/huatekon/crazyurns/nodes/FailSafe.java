package org.huatekon.crazyurns.nodes;

import org.huatekon.crazyurns.utilities.Variables;
import org.huatekon.utilities.Condition;
import org.huatekon.utilities.Utilities;
import org.powerbot.core.script.job.state.Node;
import org.powerbot.game.api.methods.interactive.Players;
import org.powerbot.game.api.methods.node.SceneEntities;
import org.powerbot.game.api.wrappers.node.SceneObject;

public class FailSafe extends Node {

	private static final int STAIRCASE = 24353;
	private SceneObject staircase = null;

	@Override
	public boolean activate() {
		return Players.getLocal().getLocation().getPlane() == 1;
	}

	@Override
	public void execute() {
		Variables.status = "Failsafe";
		staircase = SceneEntities.getNearest(STAIRCASE);
		if (staircase != null) {
			Utilities.focus(staircase);
			if (staircase.interact("Climb-down")) {
				Utilities.waitFor(new Condition() {
					@Override
					public boolean validate() {
						staircase = SceneEntities.getNearest(STAIRCASE);
						return staircase == null;
					}
				}, 4000);
			}
		}
	}

}
