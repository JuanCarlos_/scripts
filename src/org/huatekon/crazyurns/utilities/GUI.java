package org.huatekon.crazyurns.utilities;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Gui extends JFrame{

	private static final long serialVersionUID = 1L;
	private JLabel label1;
	@SuppressWarnings("rawtypes")
	private JComboBox comboBoxUrn;
	private JButton buttonStart;

	private void buttonStartActionPerformed(ActionEvent e) {
		if (e.getSource().equals(buttonStart)) {
			if (comboBoxUrn.getSelectedItem().toString()
					.equalsIgnoreCase("Cracked Cooking")) {
				Variables.URNUNF = 20349;
				Variables.URNNR = 20350;
				Variables.making = "Cracked Cooking";
				Variables.urnCategory = 1;
				Variables.urnId = Variables.URNUNF;
				Variables.urnWidgetId = 2;
				Variables.categoryName = "Cooking Urns";
				Variables.urnName = "Cracked cooking urn (unf)";
			} 
			else if (comboBoxUrn.getSelectedItem().toString()
					.equalsIgnoreCase("Cracked Fishing")) {
				Variables.URNUNF = 20319;
				Variables.URNNR = 20320;
				Variables.making = "Cracked Fishing";
				Variables.urnCategory = 2;
				Variables.urnId = Variables.URNUNF;
				Variables.urnWidgetId = 2;
				Variables.categoryName = "Fishing Urns";
				Variables.urnName = "Cracked fishing urn (unf)";
				
			} else if (comboBoxUrn.getSelectedItem().toString()
					.equalsIgnoreCase("Cracked Mining")) {
				Variables.URNUNF = 20379;
				Variables.URNNR = 20380;
				Variables.making = "Cracked Mining";
				Variables.urnCategory = 3;
				Variables.urnId = Variables.URNUNF;
				Variables.urnWidgetId = 2;
				Variables.categoryName = "Mining Urns";
				Variables.urnName = "Cracked mining urn (unf)";
			} 
			else if (comboBoxUrn.getSelectedItem().toString()
					.equalsIgnoreCase("Cracked Smithing")) {
				Variables.URNUNF = 20271;
				Variables.URNNR = 20272;
				Variables.making = "Cracked Smithing";
				Variables.urnCategory = 5;
				Variables.urnId = Variables.URNUNF;
				Variables.urnWidgetId = 2;
				Variables.categoryName = "Smithing Urns";
				Variables.urnName = "Cracked smelting urn (unf)";
			} 
			else if (comboBoxUrn.getSelectedItem().toString()
					.equalsIgnoreCase("Cracked Woodcutting")) {
				Variables.URNUNF = 20295;
				Variables.URNNR = 20296;
				Variables.making = "Cracked Woodcutting";
				Variables.urnCategory = 6;
				Variables.urnId = Variables.URNUNF;
				Variables.urnWidgetId = 2;
				Variables.categoryName = "Woodcutting Urns";
				Variables.urnName = "Cracked woodcutting urn<br> (unf)";
			} 
			else if (comboBoxUrn.getSelectedItem().toString()
					.equalsIgnoreCase("Fragile Cooking")) {
				Variables.URNUNF = 20355;
				Variables.URNNR = 20398;
				Variables.making = "Fragile Cooking";
				Variables.urnCategory = 1;
				Variables.urnId = Variables.URNUNF;
				Variables.urnWidgetId = 6;
				Variables.categoryName = "Cooking Urns";
				Variables.urnName = "Fragile cooking urn (unf)";
			} 
			else if (comboBoxUrn.getSelectedItem().toString()
					.equalsIgnoreCase("Fragile Fishing")) {
				Variables.URNUNF = 20325;
				Variables.URNNR = 20398;
				Variables.making = "Fragile Fishing";
				Variables.urnCategory = 2;
				Variables.urnId = Variables.URNUNF;
				Variables.urnWidgetId = 6;
				Variables.categoryName = "Fishing Urns";
				Variables.urnName = "Fragile fishing urn (unf)";
			} else if (comboBoxUrn.getSelectedItem().toString()
					.equalsIgnoreCase("Fragile Mining")) {
				Variables.URNUNF = 20385;
				Variables.URNNR = 20398;
				Variables.making = "Fragile Mining";
				Variables.urnCategory = 3;
				Variables.urnId = Variables.URNUNF;
				Variables.urnWidgetId = 6;
				Variables.categoryName = "Mining Urns";
				Variables.urnName = "Fragile mining urn (unf)";
			} 
			else if (comboBoxUrn.getSelectedItem().toString()
					.equalsIgnoreCase("Fragile Smithing")) {
				Variables.URNUNF = 20277;
				Variables.URNNR = 20398;
				Variables.making = "Fragile Smithing";
				Variables.urnCategory = 5;
				Variables.urnId = Variables.URNUNF;
				Variables.urnWidgetId = 6;
				Variables.categoryName = "Smithing Urns";
				Variables.urnName = "Fragile smelting urn (unf)";
			} 
			else if (comboBoxUrn.getSelectedItem().toString()
					.equalsIgnoreCase("Fragile Woodcutting")) {
				Variables.URNUNF = 20301;
				Variables.URNNR = 20398;
				Variables.making = "Fragile Woodcutting";
				Variables.urnCategory = 6;
				Variables.urnId = Variables.URNUNF;
				Variables.urnWidgetId = 6;
				Variables.categoryName = "Woodcutting Urns";
				Variables.urnName = "Fragile woodcutting urn<br> (unf)";
			} 
			else if (comboBoxUrn.getSelectedItem().toString()
					.equalsIgnoreCase("Strong Mining")) {
				Variables.URNUNF = 20397;
				Variables.URNNR = 20398;
				Variables.making = "Strong Mining";
				Variables.urnCategory = 3;
				Variables.urnId = Variables.URNUNF;
				Variables.urnWidgetId = 14;
				Variables.categoryName = "Mining Urns";
				Variables.urnName = "Strong mining urn (unf)";
			} else if (comboBoxUrn.getSelectedItem().toString()
					.equalsIgnoreCase("Strong Smelting")) {
				Variables.URNUNF = 20289;
				Variables.URNNR = 20290;
				Variables.making = "Strong Smelting";
				Variables.urnCategory = 5;
				Variables.urnId = Variables.URNUNF;
				Variables.urnWidgetId = 14;
				Variables.categoryName = "Smithing Urns";
				Variables.urnName = "Strong smelting urn (unf)";
			} else if (comboBoxUrn.getSelectedItem().toString()
					.equalsIgnoreCase("Strong Cooking")) {
				Variables.URNUNF = 20367;
				Variables.URNNR = 20368;
				Variables.making = "Strong Cooking";
				Variables.urnCategory = 1;
				Variables.urnId = Variables.URNUNF;
				Variables.urnWidgetId = 14;
				Variables.categoryName = "Cooking Urns";
				Variables.urnName = "Strong cooking urn (unf)";
			} else if (comboBoxUrn.getSelectedItem().toString()
					.equalsIgnoreCase("Strong Fishing")) {
				Variables.URNUNF = 20337;
				Variables.URNNR = 20338;
				Variables.making = "Strong Fishing";
				Variables.urnCategory = 2;
				Variables.urnId = Variables.URNUNF;
				Variables.urnWidgetId = 14;
				Variables.categoryName = "Fishing Urns";
				Variables.urnName = "Strong fishing urn (unf)";
			} else if (comboBoxUrn.getSelectedItem().toString()
					.equalsIgnoreCase("Strong Woodcutting")) {
				Variables.URNUNF = 20313;
				Variables.URNNR = 20314;
				Variables.making = "Strong Woodcutting";
				Variables.urnCategory = 6;
				Variables.urnId = Variables.URNUNF;
				Variables.urnWidgetId = 14;
				Variables.categoryName = "Woodcutting Urns";
				Variables.urnName = "Strong woodcutting urn<br> (unf)";
			} else if (comboBoxUrn.getSelectedItem().toString()
					.equalsIgnoreCase("Decorated Fishing")) {
				Variables.URNUNF = 20343;
				Variables.URNNR = 20344;
				Variables.making = "Decorated Fishing";
				Variables.urnCategory = 2;
				Variables.urnId = Variables.URNUNF;
				Variables.urnWidgetId = 18;
				Variables.categoryName = "Fishing Urns";
				Variables.urnName = "Decorated fishing urn (unf)";
			} else if (comboBoxUrn.getSelectedItem().toString()
					.equals("Decorated Cooking")) {
				Variables.URNUNF = 20373;
				Variables.URNNR = 20374;
				Variables.making = "Decorated Cooking";
				Variables.urnCategory = 1;
				Variables.urnId = Variables.URNUNF;
				Variables.urnWidgetId = 18;
				Variables.categoryName = "Cooking Urns";
				Variables.urnName = "Decorated cooking urn<br> (unf)";
			} 
			else if (comboBoxUrn.getSelectedItem().toString()
					.equalsIgnoreCase("Decorated Mining")) {
				Variables.URNUNF = 20403;
				Variables.URNNR = 20404;
				Variables.making = "Decorated Mining";
				Variables.urnCategory = 3;
				Variables.urnId = Variables.URNUNF;
				Variables.urnWidgetId = 18;
				Variables.categoryName = "Mining Urns";
				Variables.urnName = "Decorated mining urn (unf)";
			} 
			else if (comboBoxUrn.getSelectedItem().toString()
					.equals("Cooking urn")) {
				Variables.URNUNF = 20361;
				Variables.URNNR = 20362;
				Variables.making = "Cooking urn";
				Variables.urnCategory = 1;
				Variables.urnId = Variables.URNUNF;
				Variables.urnWidgetId = 10;
				Variables.categoryName = "Cooking Urns";
				Variables.urnName = "Cooking urn (unf)";
			} else if (comboBoxUrn.getSelectedItem().toString()
					.equals("Fishing urn")) {
				Variables.URNUNF = 20331;
				Variables.URNNR = 20332;
				Variables.making = "Fishing urn";
				Variables.urnCategory = 2;
				Variables.urnId = Variables.URNUNF;
				Variables.urnWidgetId = 10;
				Variables.categoryName = "Fishing Urns";
				Variables.urnName = "Fishing urn (unf)";
			} else if (comboBoxUrn.getSelectedItem().toString()
					.equals("Mining urn")) {
				Variables.URNUNF = 20391;
				Variables.URNNR = 20392;
				Variables.making = "Mining urn";
				Variables.urnCategory = 3;
				Variables.urnId = Variables.URNUNF;
				Variables.urnWidgetId = 10;
				Variables.categoryName = "Mining Urns";
				Variables.urnName = "Mining urn (unf)";
			} else if (comboBoxUrn.getSelectedItem().toString()
					.equals("Smelting urn")) {
				Variables.URNUNF = 20283;
				Variables.URNNR = 20284;
				Variables.making = "Smelting urn";
				Variables.urnCategory = 5;
				Variables.urnId = Variables.URNUNF;
				Variables.urnWidgetId = 10;
				Variables.categoryName = "Smithing Urns";
				Variables.urnName = "Smelting urn (unf)";
			} else if (comboBoxUrn.getSelectedItem().toString()
					.equals("Woodcutting urn")) {
				Variables.URNUNF = 20307;
				Variables.URNNR = 20308;
				Variables.making = "Woodcutting urn";
				Variables.urnCategory = 6;
				Variables.urnId = Variables.URNUNF;
				Variables.urnWidgetId = 10;
				Variables.categoryName = "Woodcutting Urns";
				Variables.urnName = "Woodcutting urn (unf)";
			} else if (comboBoxUrn.getSelectedItem().toString()
					.equals("Accursed urn")) {
				Variables.URNUNF = 20415;
				Variables.URNNR = 20416;
				Variables.making = "Accursed urn";
				Variables.urnCategory = 4;
				Variables.urnId = Variables.URNUNF;
				Variables.urnWidgetId = 6;
				Variables.categoryName = "Prayer Urns";
				Variables.urnName = "Accursed urn (unf)";
			} else if (comboBoxUrn.getSelectedItem().toString()
					.equalsIgnoreCase("Infernal")) {
				Variables.URNUNF = 20421;
				Variables.URNNR = 20422;
				Variables.making = "Infernal";
				Variables.urnCategory = 4;
				Variables.urnId = Variables.URNUNF;
				Variables.urnWidgetId = 10;
				Variables.categoryName = "Prayer Urns";
				Variables.urnName = "Infernal urn (unf)";
			}
			else if (comboBoxUrn.getSelectedItem().toString()
					.equalsIgnoreCase("Impious")) {
				Variables.URNUNF = 20421;
				Variables.URNNR = 20422;
				Variables.making = "Impious";
				Variables.urnCategory = 4;
				Variables.urnId = Variables.URNUNF;
				Variables.urnWidgetId = 2;
				Variables.categoryName = "Prayer Urns";
				Variables.urnName = "Impious urn (unf)";
			}
		}
		Variables.started = true;
		dispose();
	}

	@Override
	public void dispose() {
		super.dispose();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Gui() {
		label1 = new JLabel();
		comboBoxUrn = new JComboBox();
		buttonStart = new JButton();

		// ======== this ========
		setTitle("CrazyUrns by huatekon");
		setIconImage(null);
		setAlwaysOnTop(false);
		setResizable(false);
		Container contentPane = getContentPane();
		contentPane.setLayout(null);

		// ---- label1 ----
		label1.setText("Urn:");
		label1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		contentPane.add(label1);
		label1.setBounds(new Rectangle(new Point(40, 35), label1
				.getPreferredSize()));

		// ---- comboBoxUrn ----
		comboBoxUrn.setFont(new Font("Tahoma", Font.PLAIN, 14));
		comboBoxUrn.setModel(new DefaultComboBoxModel(new String[] {
				"Cracked Cooking", "Cracked Fishing", "Cracked Mining", "Cracked Smithing", "Cracked Woodcutting",
				"Fragile Cooking", "Fragile Fishing", "Fragile Mining", "Fragile Smithing", "Fragile Woodcutting",
				"Cooking urn", "Fishing urn", "Mining urn", "Smelting urn", "Woodcutting urn",
				"Strong Mining", "Strong Smelting", "Strong Cooking", "Strong Fishing", "Strong Woodcutting",
				"Decorated Cooking", "Decorated Fishing",  "Decorated Mining",
				"Impious", "Accursed urn", "Infernal"}));
		contentPane.add(comboBoxUrn);
		comboBoxUrn.setBounds(130, 35, 220,
				comboBoxUrn.getPreferredSize().height);

		// ---- buttonStart ----
		buttonStart.setText("Start");
		buttonStart.setFont(new Font("Tahoma", Font.PLAIN, 16));
		buttonStart.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				buttonStartActionPerformed(e);
			}
		});
		contentPane.add(buttonStart);
		buttonStart.setBounds(new Rectangle(new Point(130, 85), buttonStart
				.getPreferredSize()));

		{ // compute preferred size
			Dimension preferredSize = new Dimension();
			for (int i = 0; i < contentPane.getComponentCount(); i++) {
				Rectangle bounds = contentPane.getComponent(i).getBounds();
				preferredSize.width = Math.max(bounds.x + bounds.width,
						preferredSize.width);
				preferredSize.height = Math.max(bounds.y + bounds.height,
						preferredSize.height);
			}
			Insets insets = contentPane.getInsets();
			preferredSize.width += insets.right;
			preferredSize.height += insets.bottom;
			contentPane.setMinimumSize(preferredSize);
			contentPane.setPreferredSize(preferredSize);
		}
		pack();
		setLocationRelativeTo(getOwner());
	}
	
}
