package org.huatekon.crazyurns.utilities;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import org.powerbot.game.api.methods.input.Mouse;
import org.powerbot.game.api.methods.tab.Skills;
import org.powerbot.game.api.util.Time;
import org.powerbot.game.api.util.Timer;

public class Paint {

	public static void onRepaint(Graphics g) {
		if (Variables.timer == null) {
			Variables.timer = new Timer(0);
		}
		if (Variables.startxp == 0) {
			Variables.startxp = Skills.getExperience(Skills.CRAFTING);
		}
		if (Variables.level == 0) {
			Variables.level = Skills.getLevel(Skills.CRAFTING);
		}
		if (!Variables.hidepaint) {
			g.setColor(Color.WHITE);
			g.setFont(new Font("Arial", Font.BOLD, 14));
			g.drawImage(Variables.paint, 0, 390, null);
			if (Variables.making != null)
				g.drawString(Variables.making + " ("+ Variables.urnsMade + ")" , 110, 455);
			g.drawString("Time running: " + Variables.timer.toElapsedString(),
					70, 483);

			final long xpPH = (long) (3600000d / (double) Variables.timer
					.getElapsed() * (double) (Skills
					.getExperience(Skills.CRAFTING) - Variables.startxp));

			g.drawString(
					"Exp. gained: "
							+ (Skills.getExperience(Skills.CRAFTING) - Variables.startxp)
							+ " ( " + xpPH + " )", 70, 505);

			g.drawString(
					(Variables.level
							+ " + ( "
							+ (Skills.getLevel(Skills.CRAFTING) - Variables.level) + " )"),
					406, 458);
			if (Variables.status != null)
				g.drawString(Variables.status, 342, 483);

			if (xpPH != 0) {
				final long timeTillLevel = (long) (((double) ((Skills
						.getExperienceToLevel(Skills.CRAFTING,
								Skills.getLevel(Skills.CRAFTING) + 1))) * 3600000.0) / (double) xpPH);
				g.drawString(Time.format(timeTillLevel), 330, 508);
			}
		}
		else{
			g.drawRect(Variables.rect.x, Variables.rect.y, Variables.rect.width, Variables.rect.height);
		}
		g.drawLine(Mouse.getX(), 0, Mouse.getX(), 502);
		g.drawLine(0, Mouse.getY(), 762, Mouse.getY());
	}
}
