package org.huatekon.crazyurns.utilities;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import org.powerbot.game.api.util.Timer;

public class Variables {

	public static int urnsMade;
	
	/**
	 * Use to choose what category of urns to make.<br />
	 * <b>Cooking</b> = 1, <br />
	 * <b>Fishing</b> = 2, <br />
	 * <b>Mining</b> = 3, <br />
	 * <b>Prayer</b> = 4, <br />
	 * <b>Smithing</b> = 5, <br />
	 * <b>Woodcutting</b> = 6
	 */
	public static int urnCategory;
	
	/**
	 * Use to choose what kind of urn to make.<br />
	 * <b>Cooking:</b>
	 * <i>Cracked</i> = 20349, <i>Fragile = 20355</i>, <i>Cooking</i> = 20361, <i>Strong</i> = 20367, <i>Decorated</i> = 20373 <br />
	 * <b>Fishing</b>
	 * <i>Cracked</i> = 20319, <i>Fragile = 20325</i>, <i>Fishing</i> = 20331, <i>Strong</i> = 20337, <i>Decorated</i> = 20343 <br />
	 * <b>Mining</b>
	 * <i>Cracked</i> = 20379, <i>Fragile = 20385</i>, <i>Mining</i> = 20391, <i>Strong</i> = 20397, <i>Decorated</i> = 20403 <br />
	 * <b>Prayer</b>
	 * <i>Impious</i> = 20409, <i>Accursed</i> = 20415, <i>Infernal</i> = 20421 <br />
	 * <b>Smithing</b>
	 * <i>Cracked</i> = 20271, <i>Fragile = 20277</i>, <i>Smithing</i> = 20283, <i>Strong</i> = 20289 <br />
	 * <b>Woodcutting</b>
	 * <i>Cracked</i> = 20295, <i>Fragile = 20301</i>, <i>Woodcutting</i> = 20307, <i>Strong</i> = 30313 <br />
	 */
	public static int urnId;
	
	/**
	 * First slot = 2,
	 * Second slot = 6,
	 * Third slot = 10,
	 * Fourth slot = 14,
	 * Fifth slot = 18
	 */
	public static int urnWidgetId;
	public static int URNUNF;
	public static int URNNR;
	public static int startxp;
	public static int level;
	/**
	 * Total of urns to be made before starting to attach runes.
	 */
	//public static int attachAfterMade  = 14;
	//public static int totaToAttach = 14;
	/**
	 * id of urn after rune attached.
	 */
	//public static int urnCompleted = 20376;
	
	public static String urnName; 
	public static String categoryName;
	public static String status;
	public static String making;
	
	public static Timer timer;

	public static boolean stop;
	public static boolean started;
	public static boolean hidepaint;
	
	/**
	 * if <b>true</b> it will attach rune to urns.<br />
	 * if <b>false</b> it continues making urns.
	 */
	//public static boolean attach = true;

	public static BufferedImage paint;
	
	public static final Rectangle rect = new Rectangle(490, 407, 27, 27);

}
