package org.huatekon.edgesmelter.utilities;

import java.awt.image.BufferedImage;

import org.powerbot.game.api.util.Timer;
import org.powerbot.game.api.wrappers.Area;
import org.powerbot.game.api.wrappers.Tile;

public class Variables {
	
	public static Area bankArea = new Area(new Tile(0, 0, 0), new Tile(0, 0, 0));;
	public static Area furnaceArea = new Area(new Tile(0, 0, 0), new Tile(0, 0, 0));
	public static int furnace = 0;
	public static final int[] ORES = { 
		438, //tin
		440, //iron
		442, //silver
		444, //gold
		453, //coal
		447, //mith
		449, //addy
		451 }; //rune
	public static int currentore;
	public static int startxp = 0;
	public static int barsmade = 0;
	public static int level = 0;
	public static int minamountcoal = 0;
	public static boolean started;
	public static boolean random;
	public static boolean hidepaint;
	public static String status = "";
	public static String making = "";
	public static Timer timer;
	public static BufferedImage paint;
	public static int location; // 0 for al-kharid, 1 for edgeville
}
