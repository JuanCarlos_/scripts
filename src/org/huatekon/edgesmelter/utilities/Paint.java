package org.huatekon.edgesmelter.utilities;

import java.awt.Font;
import java.awt.Graphics;

import org.powerbot.game.api.methods.input.Mouse;
import org.powerbot.game.api.methods.tab.Skills;
import org.powerbot.game.api.util.Time;
import org.powerbot.game.api.util.Timer;

public class Paint {
	
	
	public static void onRepaint(Graphics g) {
		if (Variables.timer == null) {
			Variables.timer = new Timer(0);
		}
		if (Variables.startxp == 0) {
			Variables.startxp = Skills.getExperience(Skills.SMITHING);
		}
		if (Variables.level == 0) {
			Variables.level = Skills.getLevel(Skills.SMITHING);
		}
		if (!Variables.hidepaint) {
			g.drawImage(Variables.paint, 0, 390, null);
			g.setFont(new Font("Arial", Font.BOLD, 12));
			g.drawString("EdgeSmelter v4.2 by huatekon {Scripts for Pro's}",
					240, 405);
			g.setFont(new Font("Arial", Font.PLAIN, 12));
			g.drawString("Making: " + Variables.making, 320, 425);
			g.drawString("Bars/balls made: " + Variables.barsmade, 320, 440);
			g.drawString(
					"Exp. gained: "
							+ (Skills.getExperience(Skills.SMITHING) - Variables.startxp),
					320, 455);
			final long xpPH = (long) (3600000d / (double) Variables.timer.getElapsed() * (double) (Skills
					.getExperience(Skills.SMITHING) - Variables.startxp));
			g.drawString("Exp p/h: " + xpPH, 320, 470);
			g.drawString(
					"Levels gained: "
							+ (Variables.level
									+ "+("
									+ (Skills.getLevel(Skills.SMITHING) - Variables.level) + ")"),
					320, 485);
			g.drawString("Time running: " + Variables.timer.toElapsedString(), 320, 500);
			g.drawString("Status: " + Variables.status, 320, 515);
			if (xpPH != 0) {
				final long timeTillLevel = (long) (((double) ((Skills
						.getExperienceToLevel(Skills.SMITHING,
								Skills.getLevel(Skills.SMITHING) + 1))) * 3600000.0) / (double) xpPH);
				g.drawString("TTL: " + Time.format(timeTillLevel), 430, 470);
			}
		} else {

		}
		g.drawLine(Mouse.getX(), 0, Mouse.getX(), 502);
		g.drawLine(0, Mouse.getY(), 762, Mouse.getY());
	}
}
