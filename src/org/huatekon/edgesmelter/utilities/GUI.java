package org.huatekon.edgesmelter.utilities;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

import org.powerbot.game.api.wrappers.Area;
import org.powerbot.game.api.wrappers.Tile;

public class GUI extends JFrame {
	private static final long serialVersionUID = 1L;

	public GUI() {
		super("EdgeSmelter");
		initComponents();
		setLocationRelativeTo(getParent());
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
	}

	@Override
	public void dispose() {
		super.dispose();
	}

	private void button1ActionPerformed(ActionEvent e) {
		
		if(comboBox2.getSelectedItem().toString().equals("Al Kharid")){
			Variables.bankArea = new Area(new Tile(3272, 3169, 0),
					new Tile(3268, 3166, 0));
			Variables.furnaceArea = new Area(new Tile(3277, 3192, 0),
					new Tile(3272, 3187, 0));
			Variables.furnace = 76293;
			Variables.location = 0;
		} else if(comboBox2.getSelectedItem().toString().equals("Edgeville")){
			Variables.bankArea = new Area(new Tile(3099, 3500, 0),
					new Tile(3094, 3495, 0));
			Variables.furnaceArea = new Area(new Tile(3110, 3504, 0),
					new Tile(3106, 3497, 0));
			Variables.furnace = 26814;
			Variables.location = 1;
		}
		
		if (comboBox1.getSelectedItem().toString().equals("Bronze")) {
			Variables.currentore = Variables.ORES[0];
			Variables.making = "Bronze bars";
		} else if (comboBox1.getSelectedItem().toString().equals("Iron")) {
			Variables.currentore = Variables.ORES[1];
			Variables.making = "Iron bars";
		} else if (comboBox1.getSelectedItem().toString().equals("Silver")) {
			Variables.currentore = Variables.ORES[2];
			Variables.making = "Silver bars";
		} else if (comboBox1.getSelectedItem().toString().equals("Steel")) {
			Variables.currentore = Variables.ORES[4];
			Variables.making = "Steel bars";
			Variables.minamountcoal = 2;
		} else if (comboBox1.getSelectedItem().toString().equals("Cannonball")) {
			Variables.currentore = 2353;
			Variables.making = "Cannonballs";
		} else if (comboBox1.getSelectedItem().toString().equals("Gold")) {
			Variables.currentore = Variables.ORES[3];
			Variables.making = "Gold bars";
		} else if (comboBox1.getSelectedItem().toString().equals("Mith")) {
			Variables.currentore = Variables.ORES[5];
			Variables.making = "Mith bars";
			Variables.minamountcoal = 4;
		} else if (comboBox1.getSelectedItem().toString().equals("Adamant")) {
			Variables.currentore = Variables.ORES[6];
			Variables.making = "Adamant bars";
			Variables.minamountcoal = 6;
		} else {
			Variables.currentore = Variables.ORES[7];
			Variables.making = "Rune bars";
			Variables.minamountcoal = 8;
		}
		Variables.started = true;
		dispose();
	}

	private void comboBox1ActionPerformed(ActionEvent e) {
		if (comboBox1.getSelectedItem().toString().equals("Iron"))
			checkBox1.setEnabled(true);
		else
			checkBox1.setEnabled(false);
	}

	private void initComponents() {
		label1 = new JLabel();
		label2 = new JLabel();
		label3 = new JLabel();
		comboBox1 = new JComboBox<Object>();
		comboBox2 = new JComboBox<Object>();
		button1 = new JButton();
		checkBox1 = new JCheckBox();

		// ======== this ========
		setTitle("EdgeSmelter by huatekon {Scripts for Pro's}");
		Container contentPane = getContentPane();
		contentPane.setLayout(null);

		// ---- label1 ----
		label1.setText("Make");
		label1.setFont(new Font("Arial", Font.BOLD, 16));
		contentPane.add(label1);
		label1.setBounds(new Rectangle(new Point(60, 40), label1
				.getPreferredSize()));

		// label3
		label3.setText("Location: ");
		label3.setFont(new Font("Arial", Font.BOLD, 16));
		contentPane.add(label3);
		label3.setBounds(new Rectangle(new Point(60, 10), label3
				.getPreferredSize()));

		// ---- comboBox1 ----
		comboBox1.setModel(new DefaultComboBoxModel<Object>(new String[] {
				"Bronze", "Iron", "Silver", "Steel", "Cannonball", "Gold",
				"Mith", "Adamant", "Rune" }));
		comboBox1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				comboBox1ActionPerformed(e);
			}
		});
		contentPane.add(comboBox1);
		comboBox1.setBounds(new Rectangle(new Point(115, 35), comboBox1
				.getPreferredSize()));

		// ---- comboBox2
		comboBox2.setModel(new DefaultComboBoxModel<Object>(new String[] {
				"Al Kharid", "Edgeville" }));
		contentPane.add(comboBox2);
		comboBox2.setBounds(new Rectangle(new Point(150, 10), comboBox2
				.getPreferredSize()));

		// ---- button1 ----
		button1.setText("Start");
		button1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				button1ActionPerformed(e);
			}
		});
		contentPane.add(button1);
		button1.setBounds(new Rectangle(new Point(130, 95), button1
				.getPreferredSize()));

		// ---- label2 ----
		label2.setText("bars.");
		label2.setFont(new Font("Arial", Font.BOLD, 16));
		contentPane.add(label2);
		label2.setBounds(new Rectangle(new Point(210, 40), label2
				.getPreferredSize()));

		// ---- checkBox1 ----
		checkBox1.setText("Ring of forging");
		checkBox1.setEnabled(false);
		contentPane.add(checkBox1);
		checkBox1.setBounds(new Rectangle(new Point(125, 70), checkBox1
				.getPreferredSize()));

		contentPane.setPreferredSize(new Dimension(345, 165));
		pack();
		setLocationRelativeTo(getOwner());
	}

	private JLabel label1, label2, label3;;

	private JComboBox<Object> comboBox1, comboBox2;
	private JButton button1;
	private JCheckBox checkBox1;
}