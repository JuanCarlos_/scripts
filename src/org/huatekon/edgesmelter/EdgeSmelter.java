package org.huatekon.edgesmelter;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.SwingUtilities;

import org.huatekon.edgesmelter.nodes.Deposit;
import org.huatekon.edgesmelter.nodes.FailSafe;
import org.huatekon.edgesmelter.nodes.Smelt;
import org.huatekon.edgesmelter.nodes.WalkBank;
import org.huatekon.edgesmelter.nodes.WalkFurnace;
import org.huatekon.edgesmelter.nodes.Withdraw;
import org.huatekon.edgesmelter.utilities.GUI;
import org.huatekon.edgesmelter.utilities.Paint;
import org.huatekon.edgesmelter.utilities.Variables;
import org.huatekon.utilities.ClaimSpinTickets;
import org.huatekon.utilities.ProductionDialog;
import org.powerbot.core.event.events.MessageEvent;
import org.powerbot.core.event.listeners.MessageListener;
import org.powerbot.core.event.listeners.PaintListener;
import org.powerbot.core.randoms.SpinTickets;
import org.powerbot.core.script.ActiveScript;
import org.powerbot.core.script.job.Task;
import org.powerbot.core.script.job.state.Node;
import org.powerbot.core.script.job.state.Tree;
import org.powerbot.game.api.Manifest;
import org.powerbot.game.api.methods.Environment;
import org.powerbot.game.api.methods.Game;
import org.powerbot.game.api.util.Random;

@Manifest(authors = { "huatekon" },
name = "EdgeSmelter",
version = 4.2,
description = "Toggle Production Dialog ON, Smelting Smithing bars, cannonballs, Al Kharid, Edgeville",
website = "http://www.powerbot.org/community/topic/729750-edgesmelter/")
public class EdgeSmelter extends ActiveScript implements MessageListener,
		PaintListener, MouseListener {

	private Tree jobs;
	private static final ClaimSpinTickets ticketRandom = new ClaimSpinTickets();

	@Override
	public void onStart() {
		getContainer().submit(new Task() {
			@Override
			public void execute() {
				sleep(5000);
				Environment.enableRandom(SpinTickets.class, false);
			}
		});
		
		getContainer().submit(new ProductionDialog());

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				GUI g = new GUI();
				g.setVisible(true);
			}
		});
		try {
			Variables.paint = ImageIO.read(new URL(
					"http://dl.dropbox.com/u/15725670/edgesmelter.jpg"));
		} catch (IOException e) {
			System.out.println("error");
		}
	}

	@Override
	public int loop() {
		if (ticketRandom.activate()) {
			ticketRandom.execute();
			return 100;
		}
		
		if (jobs == null) {
			jobs = new Tree(new Node[] { new Deposit(), new Withdraw(),
					new WalkFurnace(), new Smelt(), new WalkBank(),
					new FailSafe() });
		}
		
		if (Game.getClientState() == Game.INDEX_MAP_LOADED) {
			final Node node = jobs.state();
			if (node != null) {
				jobs.set(node);
				getContainer().submit(node);
				node.join();
			}
		}
		return Random.nextInt(200, 300);
	}

	@Override
	public void onRepaint(Graphics g) {
		Paint.onRepaint(g);
	}

	@Override
	public void messageReceived(MessageEvent m) {
		if (m.getMessage().startsWith("You retrieve a bar")) {
			Variables.barsmade++;
		}
		if (m.getMessage().equals("You remove the cannonballs from the mould.")) {
			Variables.barsmade += 4;
		}
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		Rectangle rect = new Rectangle(0, 390, 518, 138);
		if (rect.contains(arg0.getPoint()) && !Variables.hidepaint) {
			Variables.hidepaint = true;
		} else {
			Variables.hidepaint = false;
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}
	@Override
	public void mouseExited(MouseEvent e) {
	}
	@Override
	public void mousePressed(MouseEvent e) {
	}
	@Override
	public void mouseReleased(MouseEvent e) {
	}
}
