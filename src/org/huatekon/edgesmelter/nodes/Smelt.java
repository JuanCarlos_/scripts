package org.huatekon.edgesmelter.nodes;

import org.huatekon.edgesmelter.utilities.Variables;
import org.huatekon.utilities.Condition;
import org.huatekon.utilities.Utilities;
import org.powerbot.core.script.job.state.Node;
import org.powerbot.game.api.methods.Widgets;
import org.powerbot.game.api.methods.interactive.Players;
import org.powerbot.game.api.methods.node.SceneEntities;
import org.powerbot.game.api.methods.tab.Inventory;
import org.powerbot.game.api.wrappers.node.SceneObject;
import org.powerbot.game.api.wrappers.widget.WidgetChild;

public class Smelt extends Node {

	public static WidgetChild WIDGET_ACTION_BUTTON = Widgets.get(1370)
			.getChild(38);
	public static WidgetChild WIDGET_PRODUCTION_DIALOG = Widgets.get(1251)
			.getChild(10);

	@Override
	public boolean activate() {
		return Variables.started
				&& Variables.furnaceArea.contains(Players.getLocal()
						.getLocation()) && canSmelt(Variables.currentore);
	}

	@Override
	public void execute() {
		if (Variables.minamountcoal != 0) {
			if (Inventory.getCount(Variables.ORES[4]) < Variables.minamountcoal) {
				Variables.status = "Not enough coal!";
			}
		}
		Variables.status = "Smelting";
		SceneObject furnace = SceneEntities.getNearest(Variables.furnace);
		if (furnace != null) {
			Utilities.focus(furnace);
			if (furnace.interact("Smelt")) {
				Utilities.waitFor(new Condition() {
					@Override
					public boolean validate() {
						return WIDGET_ACTION_BUTTON.isOnScreen();
					}
				}, 3000);
				if (WIDGET_ACTION_BUTTON.isOnScreen()) {
					if (WIDGET_ACTION_BUTTON.click(true)) {
						Utilities.waitFor(new Condition() {
							@Override
							public boolean validate() {
								return WIDGET_PRODUCTION_DIALOG.isOnScreen();
							}
						}, 5000);
						Utilities.waitFor(new Condition() {
							@Override
							public boolean validate() {
								Utilities.antiban();
								return !WIDGET_PRODUCTION_DIALOG.isOnScreen();
							}
						}, 100000);
					}
				}
			}
		}
	}

	public static boolean canSmelt(int ore) {
		switch (ore) {
		case 438: // bronze
			if (Inventory.getCount(ore) >= 1 && Inventory.getCount(436) >= 1)
				return true;
			break;
		case 440: // iron
			if (Inventory.getCount(ore) >= 1)
				return true;
			break;
		case 442: // silver
			if (Inventory.getCount(ore) >= 1)
				return true;
			break;
		case 453: // steel
			if (Inventory.getCount(ore) >= 2 && Inventory.getCount(440) >= 1)
				return true;
			break;
		case 2353: // cannonballs
			if (Inventory.getCount(ore) >= 1)
				return true;
			break;
		case 444: // gold
			if (Inventory.getCount(ore) >= 1)
				return true;
			break;
		case 447: // mith
			if (Inventory.getCount(ore) >= 1 && Inventory.getCount(453) >= 4)
				return true;
			break;
		case 449: // addy
			if (Inventory.getCount(ore) >= 1 && Inventory.getCount(453) >= 6)
				return true;
			break;
		case 451: // rune
			if (Inventory.getCount(ore) >= 1 && Inventory.getCount(453) >= 8)
				return true;
			break;
		}
		return false;
	}
}
