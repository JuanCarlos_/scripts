package org.huatekon.edgesmelter.nodes;

import org.huatekon.edgesmelter.utilities.Variables;
import org.huatekon.utilities.Utilities;
import org.powerbot.core.script.job.state.Node;
import org.powerbot.game.api.methods.interactive.Players;
import org.powerbot.game.api.methods.node.SceneEntities;
import org.powerbot.game.api.methods.tab.Inventory;
import org.powerbot.game.api.methods.widget.Bank;
import org.powerbot.game.api.wrappers.node.SceneObject;

public class Deposit extends Node{
	
	public static final int[] BANK_IDS = {42377, //edgeville
		76274}; //al kharid

	@Override
	public boolean activate() {
		return Variables.started && Inventory.getCount() != 0
				&& Variables.bankArea.contains(Players.getLocal().getLocation())
				&& !Smelt.canSmelt(Variables.currentore);
	}

	@Override
	public void execute() {
		Variables.status = "Depositing";
		SceneObject bank = SceneEntities.getNearest(Deposit.BANK_IDS);
		if (bank != null) {
			if(Utilities.openBank(bank))
				Bank.depositInventory();
		}
	}
}
