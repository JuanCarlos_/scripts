package org.huatekon.edgesmelter.nodes;

import org.huatekon.edgesmelter.utilities.Variables;
import org.huatekon.utilities.Condition;
import org.huatekon.utilities.Utilities;
import org.powerbot.core.script.job.state.Node;
import org.powerbot.game.api.methods.Walking;
import org.powerbot.game.api.methods.interactive.Players;
import org.powerbot.game.api.wrappers.Tile;
import org.powerbot.game.api.wrappers.map.TilePath;

public class WalkFurnace extends Node {

	private static final TilePath PATH = new TilePath(new Tile[] {
			new Tile(3270, 3168, 0), new Tile(3275, 3175, 0),
			new Tile(3279, 3184, 0), new Tile(3273, 3190, 0) }); // from bank to furnace path

	@Override
	public boolean activate() {
		return Variables.started
				&& !Variables.furnaceArea.contains(Players.getLocal()
						.getLocation()) && Smelt.canSmelt(Variables.currentore);
	}

	@Override
	public void execute() {
		Variables.status = "Walking furnace";
		if (Variables.location == 0) {
			Utilities.waitFor(new Condition() {
				@Override
				public boolean validate() {
					return !PATH.traverse();
				}
			}, 7000);
		} else {
			Walking.walk(new Tile(3109, 3500, 0));
			Utilities.waitFor(new Condition() {
				@Override
				public boolean validate() {
					return Variables.furnaceArea.contains(Players.getLocal()
							.getLocation());
				}
			}, 3000);
		}
	}
}
