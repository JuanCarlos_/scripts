package org.huatekon.edgesmelter.nodes;

import org.huatekon.edgesmelter.utilities.Variables;
import org.huatekon.utilities.Utilities;
import org.powerbot.core.script.job.state.Node;
import org.powerbot.game.api.methods.interactive.Players;
import org.powerbot.game.api.methods.node.SceneEntities;
import org.powerbot.game.api.methods.tab.Inventory;
import org.powerbot.game.api.methods.widget.Bank;
import org.powerbot.game.api.wrappers.node.SceneObject;

public class Withdraw extends Node {

	@Override
	public boolean activate() {
		return Variables.started && Inventory.getCount() == 0
				&& Variables.bankArea.contains(Players.getLocal().getLocation());
	}

	@Override
	public void execute() {
		SceneObject bank = SceneEntities.getNearest(Deposit.BANK_IDS);
		Variables.status = "Withdrawing";
		if (bank.validate()) {
			if (Utilities.openBank(bank)) 
				getOre(Variables.currentore);
			
		}
	}
	
	private void getOre(int ore){
		switch (ore) {
		case 438:
			Bank.withdraw(Variables.ORES[0], 14);
			Bank.withdraw(436, 14);
			break;
		case 440:
			Bank.withdraw(Variables.ORES[1], 28);
			break;
		case 442:
			Bank.withdraw(Variables.ORES[2], 28);
			break;
		case 453:
			Bank.withdraw(Variables.ORES[1], 9);
			Bank.withdraw(Variables.ORES[4], 0);
			break;
		case 444:
			Bank.withdraw(Variables.ORES[3], 28);
			break;
		case 447:
			Bank.withdraw(Variables.ORES[5], 5);
			Bank.withdraw(Variables.ORES[4], 0);
			break;
		case 449:
			Bank.withdraw(Variables.ORES[6], 4);
			Bank.withdraw(Variables.ORES[4], 0);
			break;
		case 451:
			Bank.withdraw(Variables.ORES[7], 3);
			Bank.withdraw(Variables.ORES[4], 0);
			break;
		case 2353:
			Bank.withdraw(ore, 28);
			break;
		}
	}
}
