package org.huatekon.edgesmelter.nodes;

import org.huatekon.edgesmelter.utilities.Variables;
import org.powerbot.core.script.job.Task;
import org.powerbot.core.script.job.state.Node;
import org.powerbot.game.api.methods.Walking;
import org.powerbot.game.api.methods.interactive.Players;
import org.powerbot.game.api.methods.tab.Inventory;
import org.powerbot.game.api.util.Random;

public class FailSafe extends Node {

	@Override
	public boolean activate() {
		return Variables.started
				&& !Variables.bankArea.contains(Players.getLocal()
						.getLocation())
				&& !Variables.furnaceArea.contains(Players.getLocal()
						.getLocation())
				&& Players.getLocal().isIdle();
	}

	@Override
	public void execute() {
		Variables.status = "Failsafe";
		if (Variables.furnaceArea.contains(Players.getLocal().getLocation())
				&& Inventory.getCount(Variables.currentore) >= 2) {
			Walking.walk(Variables.furnaceArea.getTileArray()[Random.nextInt(0,
					Variables.furnaceArea.getTileArray().length)]);
			Task.sleep(1000, 1999);
		} else {
			Walking.walk(Variables.bankArea.getTileArray()[Random.nextInt(0,
					Variables.bankArea.getTileArray().length)]);
			Task.sleep(1000, 1999);
		}
	}

}
