package mx.mexdev.scripts.smeltit.resources;

import mx.mexdev.scripts.smeltit.SmeltIt;
import org.powerbot.script.methods.Bank;
import org.powerbot.script.wrappers.Interactive;
import org.powerbot.script.wrappers.Npc;
import us.scriptwith.util.Methods;

/**
 * User: jvillegas
 * Date: 29/10/13
 * Time: 08:40 AM
 */
public class SmeltItMethods extends Methods {

    private SmeltIt script;

    public SmeltItMethods(SmeltIt script) {
        super(script.getContext());
        this.script = script;
    }

    public boolean canSmelt(){
        return ctx.backpack.select().id(script.bar.getMainOre().getId()).count() >= script.bar.getMainAmount()
                && ctx.backpack.select().id(script.bar.getSecondOre().getId()).count() >= script.bar.getSecondAmount();
    }

    @Override
    public Interactive nearestBanker() {
        for (Npc n : ctx.npcs.select().id(42377).nearest().first()) {
            return n;
        }
        return super.nearestBanker();
    }

    public void withdraw(){
        switch (script.bar.getMainOre().getId()){
            case 436: //bronze
                ctx.bank.withdraw(436, 14);
                ctx.bank.withdraw(438, 14);
                break;
            case 440: //iron
                ctx.bank.withdraw(440, Bank.Amount.ALL);
                break;
            case 442: //silver
                ctx.bank.withdraw(442, Bank.Amount.ALL);
                break;
            case 444: //gold
                ctx.bank.withdraw(444, Bank.Amount.ALL);
                break;
            case 453: //steel
                ctx.bank.withdraw(453, 9);
                ctx.bank.withdraw(440, Bank.Amount.ALL);
                break;
            case 447: //mith
                ctx.bank.withdraw(447, 5);
                ctx.bank.withdraw(453, Bank.Amount.ALL);
                break;
            case 449: //addy
                ctx.bank.withdraw(449, 4);
                ctx.bank.withdraw(453, Bank.Amount.ALL);
                break;
            case 451: //rune
                ctx.bank.withdraw(451, 3);
                ctx.bank.withdraw(453, Bank.Amount.ALL);
            case 2353: //cannonballs
                ctx.bank.withdraw(2353, Bank.Amount.ALL);
                break;
        }
    }
}
