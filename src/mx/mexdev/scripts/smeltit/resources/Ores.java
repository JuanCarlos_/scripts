package mx.mexdev.scripts.smeltit.resources;

/**
 * User: jvillegas
 * Date: 29/10/13
 * Time: 09:14 AM
 */
public enum Ores {
    COPPER(436),
    TIN(438),
    IRON(440),
    SILVER(442),
    GOLD(444),
    COAL(453),
    MITH(447),
    ADDY(449),
    RUNE(451),
    CANNONBALLS(2353);

    private int id;

    private Ores(int id){
        this.id = id;
    }

    public int getId(){
        return this.id;
    }
}
