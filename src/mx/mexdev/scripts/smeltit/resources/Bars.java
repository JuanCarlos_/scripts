package mx.mexdev.scripts.smeltit.resources;

/**
 * User: jvillegas
 * Date: 29/10/13
 * Time: 08:44 AM
 */
public enum Bars {

    BRONZE(Ores.COPPER, 1, Ores.TIN, 1),
    IRON(Ores.IRON, 1, Ores.IRON, 0),
    SILVER(Ores.SILVER, 1, Ores.SILVER, 0),
    STEEL(Ores.COAL, 2, Ores.IRON, 1),
    CANNONBALLS(Ores.CANNONBALLS, 1, Ores.CANNONBALLS, 0),
    GOLD(Ores.GOLD, 1, Ores.GOLD, 0),
    MITH(Ores.MITH, 1, Ores.COAL, 4),
    ADDY(Ores.ADDY, 1, Ores.COAL, 6),
    RUNE(Ores.RUNE, 1, Ores.COAL, 8);

    private Ores mainOre, secondOre;
    private int main, second;

    private Bars(Ores mainOre, int main, Ores secondOre, int second){
        this.mainOre = mainOre;
        this.main = main;
        this.secondOre = secondOre;
        this.second = second;
    }

    public Ores getMainOre(){
        return this.mainOre;
    }

    public int getMainAmount(){
        return this.main;
    }

    public Ores getSecondOre(){
        return this.secondOre;
    }

    public int getSecondAmount(){
        return this.second;
    }
}
