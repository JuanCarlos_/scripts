package mx.mexdev.scripts.smeltit.resources;

/**
 * User: jvillegas
 * Date: 29/10/13
 * Time: 12:00 PM
 */
public enum Locations {

    AL_KHARID(1),
    EDGEVILLE(26814);

    private int id;

    private Locations(int id){
        this.id = id;
    }

    public int getFurnaceId(){
        return this.id;
    }
}
