package mx.mexdev.scripts.smeltit.ui;

import mx.mexdev.scripts.smeltit.SmeltIt;
import mx.mexdev.scripts.smeltit.jobs.banking.DepositInventory;
import mx.mexdev.scripts.smeltit.jobs.banking.OpenBank;
import mx.mexdev.scripts.smeltit.jobs.banking.Withdraw;
import mx.mexdev.scripts.smeltit.jobs.interactive.Furnace;
import mx.mexdev.scripts.smeltit.jobs.interactive.Smelt;
import mx.mexdev.scripts.smeltit.jobs.traversal.ToBank;
import mx.mexdev.scripts.smeltit.resources.Bars;
import mx.mexdev.scripts.smeltit.resources.Locations;
import org.powerbot.script.wrappers.Tile;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * User: jvillegas
 * Date: 29/10/13
 * Time: 09:52 AM
 */
public class SmeltItGUI extends JFrame{

    public SmeltItGUI(final SmeltIt script){
        super("Furnace It!");
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        final JComboBox<Locations> locations = new JComboBox<Locations>(Locations.values()){{
            setBounds(45, 10, 150,20);
            addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    final Locations location = (Locations) getSelectedItem();
                    script.location = location;
                }
            });
        }};

        final JComboBox<Bars> bars = new JComboBox<Bars>(Bars.values()){{
            setBounds(45, 40, 150,20);
        }};

        final JButton start = new JButton("Start"){{
            setBounds(80, 105, 75, 20);
            addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    script.bar = (Bars) bars.getSelectedItem();
                    script.container().submit(new OpenBank(script));
                    script.container().submit(new DepositInventory(script));
                    script.container().submit(new Withdraw(script));
                    script.container().submit(new Furnace(script));
                    script.container().submit(new Smelt(script));
                    if(script.location == Locations.EDGEVILLE)
                        script.container().submit(new ToBank(script, new Tile(3097, 3496, 0)));
                    else
                        script.container().submit(new ToBank(script, new Tile(0, 0, 0)));

                    dispose();
                }
            });
        }};

        final JPanel content = new JPanel(null){{
            add(locations);
            add(bars);
            add(start);
        }};

        this.setSize(250, 175);
        this.setContentPane(content);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setVisible(true);
    }
}
