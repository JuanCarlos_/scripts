package mx.mexdev.scripts.smeltit;

import mx.mexdev.scripts.smeltit.resources.Bars;
import mx.mexdev.scripts.smeltit.resources.Locations;
import mx.mexdev.scripts.smeltit.resources.SmeltItMethods;
import mx.mexdev.scripts.smeltit.ui.SmeltItGUI;
import org.powerbot.script.Manifest;
import us.scriptwith.core.script.Script;

import java.awt.*;

/**
 * User: jvillegas
 * Date: 29/10/13
 * Time: 08:30 AM
 */
@Manifest(name="Smelt It!",
          authors =" huatekon",
          description = "Smelt at Edgeville or Al Kharid",
          version = 1.0)
public class SmeltIt extends Script{

    public Bars bar;
    public Locations location;
    public SmeltItMethods methods;

    public SmeltIt() {
        super();
    }

    @Override
    public void start() {
        this.methods = new SmeltItMethods(this);
        final SmeltIt script = this;
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new SmeltItGUI(script);
            }
        });
    }

    @Override
    public int poll() {
        ctx.camera.setPitch(true);
        return super.poll();
    }
}
