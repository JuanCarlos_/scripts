package mx.mexdev.scripts.smeltit.jobs.traversal;

import mx.mexdev.scripts.smeltit.SmeltIt;
import org.powerbot.script.wrappers.Interactive;
import org.powerbot.script.wrappers.Locatable;
import us.scriptwith.core.script.generic.Traversal;

/**
 * User: jvillegas
 * Date: 29/10/13
 * Time: 03:56 PM
 */
public class ToBank extends Traversal<SmeltIt>{
    public ToBank(SmeltIt script, Locatable dest) {
        super(script, dest);
    }

    @Override
    public String status() {
        return "Walking to bank";
    }

    @Override
    public boolean activate() {
        final Interactive bank = script.methods.nearestBanker();
        return !script.methods.canSmelt() && (!bank.isValid() || !bank.isOnScreen());
    }
}
