package mx.mexdev.scripts.smeltit.jobs.interactive;

import mx.mexdev.scripts.smeltit.SmeltIt;
import org.powerbot.script.lang.BasicNamedQuery;
import org.powerbot.script.wrappers.GameObject;
import us.scriptwith.core.script.generic.Interaction;

/**
 * User: jvillegas
 * Date: 29/10/13
 * Time: 11:56 AM
 */
public class Furnace extends Interaction<SmeltIt, GameObject, BasicNamedQuery<GameObject>>{
    public Furnace(SmeltIt script) {
        super(script, script.getContext().objects, "Smelt", script.location.getFurnaceId());
    }

    @Override
    public boolean activate() {
        return !ctx.widgets.get(1370, 20).isOnScreen() && script.methods.canSmelt();
    }
}
