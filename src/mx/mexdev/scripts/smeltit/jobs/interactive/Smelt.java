package mx.mexdev.scripts.smeltit.jobs.interactive;

import mx.mexdev.scripts.smeltit.SmeltIt;
import org.powerbot.script.util.Condition;
import us.scriptwith.core.job.Job;

import java.util.concurrent.Callable;

/**
 * User: jvillegas
 * Date: 29/10/13
 * Time: 03:11 PM
 */
public class Smelt extends Job<SmeltIt> {
    public Smelt(SmeltIt script) {
        super(script);
    }

    @Override
    public String status() {
        return "Smelting";
    }

    @Override
    public boolean activate() {
        return ctx.widgets.get(1370, 20).isOnScreen();
    }

    @Override
    public void execute() {
        ctx.widgets.get(1370, 20).click(true);
        Condition.wait(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return !script.methods.canSmelt();
            }
        }, 1000, 100);
    }
}
