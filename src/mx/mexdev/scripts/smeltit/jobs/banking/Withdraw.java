package mx.mexdev.scripts.smeltit.jobs.banking;

import mx.mexdev.scripts.smeltit.SmeltIt;
import org.powerbot.script.wrappers.Item;
import us.scriptwith.core.job.Job;

/**
 * User: jvillegas
 * Date: 29/10/13
 * Time: 10:21 AM
 */
public class Withdraw extends Job<SmeltIt> {
    public Withdraw(SmeltIt smeltIt) {
        super(smeltIt);
    }

    @Override
    public String status() {
        return "Withdraw";
    }

    @Override
    public boolean activate() {
        return ctx.bank.isOpen()
               && ctx.bank.select().id(script.bar.getMainOre().getId()).count() > 0
               && !script.methods.canSmelt();
    }

    @Override
    public void execute() {
        script.methods.withdraw();
    }
}
