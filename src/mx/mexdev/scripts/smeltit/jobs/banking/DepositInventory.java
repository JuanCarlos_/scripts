package mx.mexdev.scripts.smeltit.jobs.banking;

import mx.mexdev.scripts.smeltit.SmeltIt;
import us.scriptwith.core.job.Job;

/**
 * User: jvillegas
 * Date: 29/10/13
 * Time: 08:36 AM
 */
public class DepositInventory extends Job<SmeltIt>{

    public DepositInventory(SmeltIt script) {
        super(script);
    }

    @Override
    public String status() {
        return "Banking";
    }

    @Override
    public boolean activate() {
        return ctx.bank.isOpen() && ctx.backpack.select().count() != 0 && !script.methods.canSmelt();
    }

    @Override
    public void execute() {
        ctx.bank.depositInventory();
    }
}
