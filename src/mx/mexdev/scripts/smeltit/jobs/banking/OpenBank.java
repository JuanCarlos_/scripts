package mx.mexdev.scripts.smeltit.jobs.banking;

import mx.mexdev.scripts.smeltit.SmeltIt;
import org.powerbot.script.wrappers.Interactive;
import org.powerbot.script.wrappers.Locatable;
import us.scriptwith.core.job.Job;

/**
 * User: jvillegas
 * Date: 29/10/13
 * Time: 08:36 AM
 */
public class OpenBank extends Job<SmeltIt>{

    private Interactive banker;

    public OpenBank(SmeltIt smeltIt) {
        super(smeltIt);
    }

    @Override
    public String status() {
        return "Banking";
    }

    @Override
    public boolean activate() {
        banker = script.methods.nearestBanker();
        return !ctx.bank.isOpen() && banker != null && banker.isOnScreen()
                && !script.methods.canSmelt();
    }

    @Override
    public void execute() {
        ctx.camera.turnTo((Locatable) banker);
        ctx.bank.open();
    }
}
